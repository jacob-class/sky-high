﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class EnemyHandler : MonoBehaviour
{
    public bool debugMode = false;                      //if we are debugging the enemy behavior..

    RoomNode assignedRoom;                              //the room the enemy is assigned to
    Vector3 spawnPointPosition;                         //the place where the enemy spawns at


    //when the object starts at runtime check to see if the object is being debugged
    private void Start()
    {
        //if we are debugging this enemy... initalize with bogus variables
        if (debugMode)
        {
            Debug.Log("Debug Mode is on for gameObject: " + gameObject.name.ToString());
            Init(null, gameObject.transform.position);
        }
    }

    //Initialize the enemy scripts based on if we are debugging or not
    public void Init(RoomNode roomAssignedTo, Vector3 spawnPointAssignedTo)
    {
        assignedRoom = roomAssignedTo;
        spawnPointPosition = spawnPointAssignedTo;

        if (GetComponent<enemyMovement>() != null)
            GetComponent<enemyMovement>().Init(debugMode);
        if (GetComponent<enemyAttack>() != null)
            GetComponent<enemyAttack>().Init(debugMode);
        if (GetComponent<Animation_Test>() != null)
            GetComponent<Animation_Test>().Init(debugMode);
        if (GetComponent<spiderAI>() != null)
            GetComponent<spiderAI>().Init(debugMode);
        if (GetComponent<skullAI>() != null)
            GetComponent<skullAI>().Init(debugMode);
    }

    //resets the enemy to the spawnPoint
    void ResetPosition()
    {
        transform.position = spawnPointPosition;
        transform.rotation = new Quaternion(0, 0, 0, 0);
    }

    //resets the enemy back to its original position and gives the enemy full health
    void ResetEnemy()
    {
        //    insert code on giving the enemy full health and reset values
        ResetPosition();
    }

    //activates the enemy in the room and resets their position
    public void ActivateEnemy()
    {
        ResetEnemy();
        gameObject.SetActive(true);
    }

    //deactivates the enemy in the room
    public void DeactivateEnemy()
    {
        gameObject.SetActive(false);
    }

    //when this enemy is destroyed, remove it from the enemy list
    public void OnDestroy()
    {
        if (!debugMode)
            assignedRoom.RemoveEnemy(gameObject);
    }

}
