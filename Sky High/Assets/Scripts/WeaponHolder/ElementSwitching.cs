﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementSwitching : MonoBehaviour
{
    public string[] elementList = {"Fire","Ice","Lightning"};
    public int SelectedElement { get; private set; } = 0;
    public int selectedElem;
    public string swapButton;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        selectedElem = SelectedElement;

        if (Input.GetKeyDown(swapButton))
        {
            if (SelectedElement >= elementList.Length)
            {
                SelectedElement = 0;
            }
            else
            {
                SelectedElement += 1;
            }
        }
    }
}
