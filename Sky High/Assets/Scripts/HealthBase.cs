﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HealthBase : MonoBehaviour
{
    Animator anim;
    public float maxHealth = 100;
    //[HideInInspector]
    public float currentHealth;
    public float FlashTime = .2f;

    //Resistances are percent based, 100 = a 100% reduction, -100 = 100% extra damage
    public float fireResistance = 0;
    public float iceResistance = 0;
    public float lightningResistance = 0;

    //If these are true, the target can receive that status effect 
    public bool canProcBurn;
    public bool canProcFreeze;
    public bool canProcShock;

    //[SerializeField]
    private float burnTickDamage = 5;
    //[SerializeField]
    private int burnTicks = 5;
    //[SerializeField]
    private float timeBtwBurnTicks = .5f;

    //[SerializeField]
    private float freezeTickDamage = 4;
    //[SerializeField]
    private int freezeTicks = 4;
    //[SerializeField]
    private float timeBtwFreezeTicks = 1f;

    //[SerializeField] 
    private float shockTickDamage = 30;
    //[SerializeField] 
    private int shockTicks = 1;
    //[SerializeField] 
    private float timeBtwShockTicks = 1;
    //[SerializeField] 
    private float shockCooldown = 5;
    private float shockTimer;


    [SerializeField] public GameObject body; //Holds the gameObject with the the current characetr/item's mesh renderer 
    private SkinnedMeshRenderer characterMesh;
    private Color originalColor;
    private Color flashColor = Color.red;
    private bool isDead;

    void Start()
    {
        characterMesh = body.GetComponent<SkinnedMeshRenderer>();

        if (gameObject.tag != "StoneMonster")
        {
            anim = GetComponent<Animator>();
        }
        
        //originalColor = characterMesh.material.color;
    }

    void OnEnable()
    {
        isDead = false;
        currentHealth = maxHealth;
        shockTimer = 0;
    }

    void Update()
    {
        if(isDead){ return; }
        shockTimer -= Time.deltaTime;
    }

    public virtual void takeDamage(float damage)
    {
        currentHealth -= damage;
        StopCoroutine(DamageFlash());
        StartCoroutine(DamageFlash());

        if (currentHealth <= 0)
        {
            isDead = true;
            Die();
        }
    }

    public void takeFireDamage(float damage, float statusChance)
    {
        float modifier = fireResistance / 100;
        float trueDamage = damage - (damage * modifier);
        takeDamage(trueDamage);
        float statusThreshold = Random.Range(1, 101);
        //Debug.Log("Threshold was " + statusThreshold + ", Status chance was " + statusChance);
        if (!isDead && canProcBurn && (statusThreshold - statusChance) <= 0)
        {
            StopCoroutine(ProcBurn());
            StartCoroutine(ProcBurn());
        }
    }

    public void takeIceDamage(float damage, float statusChance)
    {
        float modifier = iceResistance / 100;
        float trueDamage = damage - (damage * modifier);
        takeDamage(trueDamage);
        float statusThreshold = Random.Range(1, 101);
        //Debug.Log("Threshold was " + statusThreshold + ", Status chance was " + statusChance);
        if (!isDead && canProcFreeze && (statusThreshold - statusChance) <= 0)
        {
            StopCoroutine(ProcFreeze());
            StartCoroutine(ProcFreeze());
        }
    }

    public void takeLightningDamage(float damage, float statusChance)
    {
        float modifier = lightningResistance / 100;
        float trueDamage = damage - (damage * modifier);
        takeDamage(trueDamage);
        float statusThreshold = Random.Range(1, 101);
        //Debug.Log("Threshold was " + statusThreshold + ", Status chance was " + statusChance);
        if (!isDead && canProcShock && shockCooldown < 0 && (statusThreshold - statusChance) <= 0)
        {
            StopCoroutine(ProcShock());
            StartCoroutine(ProcShock());
        }
    }

    IEnumerator ProcBurn()
    {
        for (int i = 0; i < burnTicks; i++)
        {
            takeFireDamage(burnTickDamage, 0);
            yield return new WaitForSeconds(timeBtwBurnTicks);
        }
    }

    IEnumerator ProcFreeze()
    {
        for (int i = 0; i < freezeTicks; i++)
        {
            takeIceDamage(freezeTickDamage, 0);
            yield return new WaitForSeconds(timeBtwFreezeTicks);
        }
    }

    IEnumerator ProcShock()
    {
        for (int i = 0; i < shockTicks; i++)
        {
            takeLightningDamage(shockTickDamage, 0);
            yield return new WaitForSeconds(timeBtwShockTicks);
        }
        shockTimer = shockCooldown;
    }


    IEnumerator DamageFlash()
    {
        characterMesh.material.color = flashColor;
        yield return new WaitForSeconds(FlashTime);
        characterMesh.material.color = originalColor;

    }

    //checks to see if the monster/player is dead
    public bool IsThisDyingOrDead()
    {
        if (isDead)
            return true;

        if (currentHealth <= 0)
            return true;

        return false;
    }

    //kills the player/monster
    public virtual void Die()
    {
        StopAllCoroutines();


        //if the player is dying
        if (GetComponent<PlayerHP>() != null)
        {

        }
        //if the enemy is dying
        else if (GetComponent<enemyHP>() != null)
        {
            if (gameObject.tag != "StoneMonster" && gameObject.tag != "Skeleton" && gameObject.tag != "Skull")
            {
                anim.SetBool("Death", true);
            }

            Destroy(gameObject, 1);
        }
    }
}
