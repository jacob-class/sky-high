﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
public class ProjectileHandler : MonoBehaviour
{
    private bool initalized = false;
    private bool addingData = false;
    private bool removingData = false;
    private bool needToClear = false;


    List<GameObject> activeProjectiles;                 //a list of the currently active projectiles
    Queue<GameObject> additionQueue;                    //a queue of projectiles waiting to be added
    Queue<GameObject> removalQueue;                     //a queue of projectiles waiting to be removed

    //sets up the projectile handler
    public void Init()
    {
        activeProjectiles = new List<GameObject>();
        initalized = true;
    }


    private void Update()
    {
        AddQueueToList();
        RemoveQueueFromList();

        if (needToClear)
            ClearAllProjectiles();
    }

    //adds each projectile to active projectile list
    void AddQueueToList()
    {
        addingData = true;
        while (additionQueue.Count != 0)
        {
            activeProjectiles.Add(additionQueue.Dequeue());
        }
        addingData = false;
    }

    //takes all queued up removal objects and removes them from the active projecties
    void RemoveQueueFromList()
    {
        removingData = true;
        while (removalQueue.Count != 0)
        {
            GameObject projectileToBeRemoved = removalQueue.Dequeue();
            activeProjectiles.Remove(projectileToBeRemoved);
            Destroy(projectileToBeRemoved);
        }
        removingData = false;
    }

    //sets the handler to clear the queue the first oppertunity
    public void SetProjectilesToBeCleared()
    {
        needToClear = true;
    }

    //clear all projectiles to ensure that they dont stay active when going from room to room
    void ClearAllProjectiles()
    {
        //if there are no projectiles to clear, return
        if (activeProjectiles.Count == 0)
        {
            needToClear = false;
            return;
        }

        needToClear = true;

        if (addingData || removingData)
            return;

        //loop through each projectile on the list... only remove the ones that are not already marked for removal
        foreach (GameObject projectile in activeProjectiles)
        {
            if (addingData || removingData || projectile == null)
                return;

            if (projectile.GetComponent<ProjectileShotBase>() != null)
            {
                if (!projectile.GetComponent<ProjectileShotBase>().markedForRemoval)
                    RemoveProjectileFromActive(projectile);
            }
            else if (projectile.GetComponent<PlacementShotBase>() != null)
            {
                if (!projectile.GetComponent<PlacementShotBase>().markedForRemoval)
                    RemoveProjectileFromActive(projectile);
            }
            else
                Debug.Log("Error: trying to remove a projectile that does not have a base!", projectile);
        }

        needToClear = false;
    }

    //adds a projectile to the add queue
    public void AddProjectile(GameObject newProjectile)
    {
        if (newProjectile != null)
            additionQueue.Enqueue(newProjectile);
    }

    //adds a projectile to the projectile removal queue
    public void RemoveProjectile(GameObject projectileToBeRemoved)
    {
        if (projectileToBeRemoved != null)
            removalQueue.Enqueue(projectileToBeRemoved);
    }

    //removes a single projectile from the active projectiles, then destroys it
    public void RemoveProjectileFromActive(GameObject projectileToBeRemoved)
    {
        if (projectileToBeRemoved != null)
        {
            activeProjectiles.Remove(projectileToBeRemoved);
            Destroy(projectileToBeRemoved);
        }
    }
}
*/
