﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapHandler : MonoBehaviour
{
    public int floorNumber;
    Floor currentFloor;
    [HideInInspector] public GameObject currentRoom;

    public void Init()
    {
        PrefabContainer prefabs = PrefabContainer.instance;

        floorNumber = 0;
        currentFloor = new Floor();
        CreateNewFloor(5, 5, Random.Range(3, 5), 4, 
            prefabs.rooms, prefabs.walls, prefabs.enemies);
    }

    //creates a new floor with the specified statistics
    public void CreateNewFloor(int maxRoomsX, int maxRoomsY, int numberOfRooms, int numberOfSpecialRooms, 
        List<GameObject> floorRoomPrefabs, List<GameObject> floorWallPrefabs, List<GameObject> enemyPrefabs)
    {
        floorNumber++;
        //create a floor layout
        currentFloor.Init(maxRoomsX, maxRoomsY, numberOfRooms,
            numberOfSpecialRooms, floorRoomPrefabs, floorWallPrefabs, enemyPrefabs);
        //assign the start room as the current room
        currentRoom = currentFloor.GetRoomObject(RoomType.START);
        currentRoom.GetComponent<RoomNode>().ActivateRoom();
    }

    //transition the player from one room to another
    public void RoomTransition(WallNode wallNodeDestination)
    {
        GameHandler.instance.RoomTransition(wallNodeDestination);
    }

    //destroys all rooms in the current floor and assigns the maphandler a new floor
    public void DestroyCurrentFloor()
    {
        currentRoom = null;
        currentFloor.DestroyAllRooms();
        currentFloor = new Floor();
    }
}
