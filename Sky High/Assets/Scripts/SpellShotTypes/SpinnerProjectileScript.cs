﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnerProjectileScript : MonoBehaviour
{
    public int currentElement { get; set; }
    public float damage { get; set; }
    public float statusChance { get; set; }
    //public GameObject hitFX;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            DealDamage(other);
            deactivatePrefab();
        }
        if (other.tag == "Environement")
        {
            deactivatePrefab();
        }
    }

    void deactivatePrefab()
    {
        //hitFX.SetActive(true);
        gameObject.SetActive(false);
    }

    void DealDamage(Collider other)
    {
        var targetHP = other.GetComponent<HealthBase>();
        if (currentElement == 0)
        {
            targetHP.takeFireDamage(damage, statusChance);
        }
        else if (currentElement == 1)
        {
            targetHP.takeIceDamage(damage, statusChance);
        }
        else if (currentElement == 2)
        {
            targetHP.takeLightningDamage(damage, statusChance);
        }
        Debug.Log("Dealt " + damage + " damage with " + statusChance + " staus chance");
    }
}
