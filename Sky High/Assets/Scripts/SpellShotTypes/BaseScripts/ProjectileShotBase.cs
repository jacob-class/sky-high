﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ProjectileShotBase : MonoBehaviour
{
    public int currentElement { get; set; }
    public float damage { get; set; }
    public float speed { get; set; }
    public float statusChance { get; set; }
    public float activeTime { get; set; }
    public float prefabLifespan { get; set; }
    protected bool isDisabled = false;
    public bool markedForRemoval { get; private set; }
    //public GameObject hitFX;

    public virtual void Setup(float iDamage, float iSpeed, float iStatusChance, float iActiveTime, float iPrefabLifespan)
    {
        isDisabled = false;
        damage = iDamage;
        speed = iSpeed;
        statusChance = iStatusChance;
        activeTime = iActiveTime;
        prefabLifespan = iPrefabLifespan;

        Invoke("DeactivatePrefab", activeTime);
    }

    // Update is called once per frame
    public void Update()
    {
        if (isDisabled)
        {
            if (!markedForRemoval)
            {
                Invoke("RemovePrefab", prefabLifespan);
                markedForRemoval = true;
            }

            return;
        }

        transform.position += transform.forward * (speed * Time.deltaTime);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            DealDamage(other);
            DeactivatePrefab();
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("SolidObject"))
        {
            DeactivatePrefab();
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Wall"))
        {
            DeactivatePrefab();
        }
    }

    public virtual void DeactivatePrefab()
    {
        isDisabled = true;
        //hitFX.SetActive(true);
        gameObject.SetActive(false);
    }

    public void RemovePrefab()
    {
        /*
        //remove the projectile from the projectile handler
        if (GameHandler.instance != null)
            GameHandler.instance.RemoveProjectile(gameObject);
        else
            Destroy(gameObject);
        */

        Destroy(gameObject);
    }

    public virtual void DealDamage(Collider other)
    {
        //check to see if the object can take damage
        if (!ThisCanTakeDamage(other))
            return;

        var targetHP = other.GetComponent<HealthBase>();
        if (currentElement == 0)
        {
            targetHP.takeFireDamage(damage, statusChance);
        }
        else if (currentElement == 1)
        {
            targetHP.takeIceDamage(damage, statusChance);
        }
        else if (currentElement == 2)
        {
            targetHP.takeLightningDamage(damage, statusChance);
        }
    }

    //checks to see if the object can be hit and if there is still HP left
    bool ThisCanTakeDamage(Collider collisionObject)
    {
        if (collisionObject.GetComponent<HealthBase>() == null)
            return false;

        return !collisionObject.GetComponent<HealthBase>().IsThisDyingOrDead();
    }

}
