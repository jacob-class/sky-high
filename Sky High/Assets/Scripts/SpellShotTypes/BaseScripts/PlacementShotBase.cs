﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlacementShotBase : MonoBehaviour
{
    public int currentElement { get; set; }
    public float damage { get; set; }
    public float statusChance { get; set; }
    public float range { get; set; }
    public int maxTargets { get; set; }
    public float activeTime { get; set; }
    private float trueActiveTime;
    public float prefabLifespan;
    private bool isDisabled;
    public bool markedForRemoval {get; private set;}
    public LayerMask layerMaskToCheck { get; set; }

    private bool detonated;

    [SerializeField] private GameObject shotVFX;
    [SerializeField] private GameObject projectileToPlace;


    void Awake()
    {

    }

    public virtual void Setup(float iDamage, float iRange, float iStatusChance, float iActiveTime, float iPrefabLifespan)
    {
        damage = iDamage;
        range = iRange;
        statusChance = iStatusChance;
        activeTime = iActiveTime;
        prefabLifespan = iPrefabLifespan;

        trueActiveTime = activeTime + 4;
        detonated = false;
        setupAOEProjectile();

        Invoke("Detonate", activeTime);
        Invoke("DeactivatePrefab", trueActiveTime);
    }

    // Update is called once per frame
    public void Update()
    {
        if (isDisabled)
        {
            if (!markedForRemoval)
            {
                Invoke("RemovePrefab", prefabLifespan);
                markedForRemoval = true;
            }

            return;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            Detonate();
        }
    }

    public virtual void DeactivatePrefab()
    {
        isDisabled = true;
        gameObject.SetActive(false);
    }

    public void RemovePrefab()
    {
        /*
        //remove the projectile from the projectile handler
        if (GameHandler.instance != null)
            GameHandler.instance.RemoveProjectile(gameObject);
        else
            Destroy(gameObject);
        */

        Destroy(gameObject);

    }


    void setupAOEProjectile()
    {
        var proj = projectileToPlace.GetComponent<AOEProjectileBase>();
        proj.maxTargets = maxTargets;
        proj.LayerMaskToCheck = layerMaskToCheck;
        proj.Setup(damage, statusChance, range, currentElement);
    }

    public virtual void Detonate()
    {
        if (detonated) { return; }

        detonated = true;
        if (shotVFX != null)
        {
            shotVFX.SetActive(false);
        }
        projectileToPlace.SetActive(true);
    }
}
